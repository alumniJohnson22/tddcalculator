package com.m3.training.calculator;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {
	
	private Calculator objectUnderTest;
	
	@BeforeAll
	static void setupBeforeAll() {
		System.out.println("Once before all tests");
	}
	
	@AfterAll
	static void teardownAfterAll() {
		System.out.println("Once after all tests");
	}
	
	@BeforeEach
	void setup() {
		objectUnderTest = new Calculator();
	}
	
	@AfterEach
	void teardown() {
		System.out.println("Once after each test");
	}
	
	//TODO This is not a real test
	@Test
	void test_Calculator_constructor() {
		assertNotNull(objectUnderTest);
	}
	
	@Test
	void test_Calculator_evalNullString() {
		String input = null;
		String msg = "Null input did not generate IllegalArgumentException.";
		assertThrows(IllegalArgumentException.class, 
					() -> { objectUnderTest.eval(input);},
					msg);	
	}	
	
	
	
	
	
	

}
