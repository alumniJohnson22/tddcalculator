package com.m3.training.calculator;

public class DemoNull {

	public DemoNull() {

	}

	public Double eval(Double operand1, Double operand2) {
		try {
			return resultFromElsewhere(operand1, operand2);
		}
		catch (NullPointerException e) {
			String msg = "This method cannot process null arguments.";
			throw new IllegalArgumentException(msg, e);
		}
	}
	
	public Double resultFromElsewhere(Double operand1, Double operand2) {
		// TODO unboxing and Autoboxing
		if (operand1 == null || operand2 == null) {
			throw new IllegalArgumentException("Cannot parse nulls");		}
		return operand1 * operand2;		
	}

}
